/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import com.mysql.jdbc.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anissa
 */
public class DatabaseUtilities {

    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

            return null;
        }
        Connection connection = null;
        try {
            connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ConferenceDuJeudi", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }
 public static ResultSet exec(String sql) throws SQLException {
     ResultSet resultat=null;
     Statement stmt=null;
     Connection c=getConnexion();
    stmt = c.createStatement();
        resultat = stmt.executeQuery(sql);
        return resultat;
     }
     }

 