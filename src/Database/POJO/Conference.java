/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import static Database.DatabaseUtilities.getConnexion;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Anissa
 */
public class Conference {

    int i = 0;

    //decalaration des attributs
    private int idConference;
    private String nomConference;
    private String titreConference;
    private Calendar dateConference;
    private int idConferencier;
    private int idSalle;
    private int idTheme;
    private Date sqlDate;

//creation du constructeur vide de la classe conference
    public Conference() {

    }
//creation d'un constructeur avec 3 parametre

    public Conference(int _idConference, String _titreConference, int _idConferencier) {
//super(_idConference,_titreConference,_idConferencier);
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;

    }

    public Conference(int _idConference, String _titreConference, Calendar _dateConference, int _idConferencier, int _idSalle, int _idTheme, String _nomConference) {
//super(_idConference,_titreConference,_dateConference,_idConferencier,_idSalle,_idTheme);
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;
        this.dateConference = _dateConference;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
        this.nomConference = _nomConference;

    }

    public Conference(String titre, Calendar cal, int idConference, int idSalle, int idTheme) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }

    public int getIdConferencier() {
        return this.idConferencier;
    }

    public void setIdConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }

    public String getTitreConference() {
        return this.titreConference;
    }

    public void setTitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }

    public String getNomConference() {
        return this.nomConference;
    }

    public void setNomConference(String _nomConference) {
        this.nomConference = _nomConference;
    }

    public Calendar getDateConference() {
        return this.dateConference;
    }

    public void setDateConference(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }

    public int getIdSalle() {
        return this.idSalle;
    }

    public void setIdSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }

    public int getIdTheme() {
        return this.idTheme;
    }

    public void setIdTheme(int _idTheme) {
        this.idTheme = _idTheme;
    }

    public String getDateString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(dateConference.getTime());

    }

    public void insert() {
        PreparedStatement ps = null;
        //connexion a ma base de donnée
        Connection _c = getConnexion();
        //requete d'insertion de données
        String query = "INSERT INTO conference" + "(titreConference,dateConference,idConferencier,idSalle,idTheme)" + "Values(?,?,?,?,?)";
        try {
            ps = _c.prepareStatement(query);
            ps.setString(1, this.getTitreConference());
            ps.setDate(2, this.getSqlDate());
            ps.setInt(3, this.getIdConferencier());
            ps.setInt(4, this.getIdSalle());
            ps.setInt(5, this.getIdTheme());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public java.sql.Date getSqlDate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
