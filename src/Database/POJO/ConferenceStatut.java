/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import java.util.Calendar;

/**
 *
 * @author Anissa
 */
public class ConferenceStatut {
    //décalaration des parametres
    private int idConference;
    private int idStatut;
    private Calendar dateStatutConference;
    //Constructeur avec parametres entiers
    public ConferenceStatut(int _idConference,int _idStatut,Calendar _dateStatutConference){ 
     this.idConference=_idConference;
     this.idStatut=_idStatut;
     this.dateStatutConference=_dateStatutConference;
        
    }
       public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
    
       public int getIdStatut() {
        return this.idStatut;
    }

    public void setIdStatut(int _idStatut) {
        this.idStatut = _idStatut;
    }
      public Calendar getDateStatutConference() {
        return this.dateStatutConference;
    }

    public void setDateStatutConference(Calendar _dateStatutConference) {
        this.dateStatutConference = _dateStatutConference;
    }
    
    
}
