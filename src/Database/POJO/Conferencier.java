/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Anissa
 */
public class Conferencier {
    //declaration des attributs
    private int idConferencier;
    private int idConference;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private String blocNoteConferencier;
   
    //Constructeur vide de la classe 
    public Conferencier(){
        
    }
    //Creation de conferencier avec 3 parametres
    public Conferencier(int _idConferencier,String _nomPrenomConferencier,int _idConference ){
        this.idConferencier=_idConferencier;
        this.nomPrenomConferencier=_nomPrenomConferencier;
        this.idConference=_idConference;
     }
    //constructeur avec tous les parametres
        public Conferencier(int _idConferencier,String _nomPrenomConferencier,boolean _conferencierInterne,int _idConference, String _blocNoteConferencier ){
        this.idConferencier=_idConferencier;
        this.nomPrenomConferencier=_nomPrenomConferencier;
        this.conferencierInterne=_conferencierInterne;
        this.idConference=_idConference;
        this.blocNoteConferencier=_blocNoteConferencier;
        
     }
    public int getIdConferencier(){
        return this.idConferencier;
    }
    
    public void setIdConferencier(int _idConferencier){
        this.idConferencier=_idConferencier;
    }
        public String getNomPrenomConferencier(){
        return this.nomPrenomConferencier;
    }
    
    public void setNomPrenomConferencier(String _nomPrenomConferencier){
        this.nomPrenomConferencier=_nomPrenomConferencier;
    }
         public int getidConference(){
        return this.idConference;
    }
     public void setIdConference (int _idConference){
        this.idConference= _idConference;
    }
    public void setConferencierInterne (boolean _ConferencierInterne){
        this.conferencierInterne=_ConferencierInterne;
    }
    
  public String getBlocNoteConferencier (){
        return this.blocNoteConferencier;       
    }
       public void setBlocNoteConferencier (String _blocNoteConferencier){
        this.blocNoteConferencier=_blocNoteConferencier;
    }
       
  
    
}
