/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

import java.util.Calendar;

/**
 *
 * @author Anissa
 */
public class InscriptionStatut {

    //declaration des attributs      
    private int idInscriptionStatut;
    private int idStatut;
    private Calendar dateStatutInscription;

    //creation constructeur InscriptionStatut avec tous les parametres 
    public InscriptionStatut(int _IdInscriptionStatut, int _idStatut, Calendar _DateStatutInscription) {
        this.idInscriptionStatut = _IdInscriptionStatut;
        this.idStatut = _idStatut;
        this.dateStatutInscription = _DateStatutInscription;
    }
     public int getIdInscriptionStatut() {
        return this.idInscriptionStatut;
    }

    public void setIdInscriptionStatut(int _idInscritptionStatut) {
        this.idInscriptionStatut = _idInscritptionStatut;
    }
         public int getIdStatut() {
        return this.idStatut;
    }

    public void setIdStatut(int _idStatut) {
        this.idStatut = _idStatut;
    }
       public Calendar getDateStatutInscription() {
        return this.dateStatutInscription;
    }

    public void setDateStatutInscrition(Calendar _DateStatutInscription) {
        this.dateStatutInscription = _DateStatutInscription;
    }
    

    

}
