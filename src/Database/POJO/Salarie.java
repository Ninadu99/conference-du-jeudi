/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Anissa
 */
public class Salarie {

    //DEC ATTRIBU
    private int idSalarie;
    private String nomPrenomSalarie;

    public Salarie(int _idSalarie, String _nomPrenomSalarie) {

        this.idSalarie = _idSalarie;
        this.nomPrenomSalarie = _nomPrenomSalarie;
    }

    public int getIdSalarie() {
        return this.idSalarie;
    }

    public void setIdSalarie(int _idSalarie) {
        this.idSalarie = _idSalarie;

    }
     public String getNomPrenomSalarie() {
        return this.nomPrenomSalarie;
    }

    public void setNomPrenomSalarie(String _nomPrenomSalarie) {
        this.nomPrenomSalarie = _nomPrenomSalarie;    }
        
}
    
