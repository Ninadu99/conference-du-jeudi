/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;



/**
 *
 * @author Anissa
 */
public class Salle {
    //DECLARATION DES ATTRIBUTS
 private int idSalle;
 private int nbPlaceSalle;
 private String nomSalle;
 //Constructeur Salle avec tous les parametres
 public Salle(int _idSalle,int _nbPlaceSalle,String _nomSalle){
     this.idSalle=_idSalle;
     this.nbPlaceSalle=_nbPlaceSalle;
     this.nomSalle=_nomSalle;
 }
   public int getIdSalle() {
        return this.idSalle;
    }

    public void setIdSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }
    public int getNbPlaceSalle() {
        return this.nbPlaceSalle;
    }

    public void setNbPlaceSalle(int _nbPlaceSalle) {
        this.nbPlaceSalle = _nbPlaceSalle;
    }
    public String getNomSalle() {
        return this.nomSalle;
    }

    public void setNomSalle(String _nomSalle) {
        this.nomSalle = _nomSalle;
    }
    
    
    
}
