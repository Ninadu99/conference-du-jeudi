/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database.POJO;

/**
 *
 * @author Anissa
 */
public class Theme {
     //declaration des attributs      
    private int idTheme;
    private String designationTheme;
    //CREATION DE CONSTRUCTEUR
    public Theme(int _idTheme,String _designationTheme){
        this.idTheme=_idTheme;
        this.designationTheme=_designationTheme;
    }
     public int getIdTheme() {
        return this.idTheme;
    }

    public void setIdTheme(int _idTheme) {
        this.idTheme = _idTheme;
    }
     public String getDesignationTheme() {
        return this.designationTheme;
    }

    public void setDesignationnTheme(String _designationTheme) {
        this.designationTheme = _designationTheme;    }
        
}
