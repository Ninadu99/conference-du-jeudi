package LesConferencesDuJeudi;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class CustomWindow extends JFrame implements ActionListener {
    

    private JMenuBar menuBar = new JMenuBar();
    private JMenu menu1 = new JMenu("Fichier");
    private JMenu menu2 = new JMenu("Conférences");

    private JMenuItem item1;
    private JMenuItem item2;
    private JMenuItem item3;

    public CustomWindow(String _title) {


        this.setJMenuBar(menuBar);
        this.menuBar.add(menu1);
        this.menuBar.add(menu2);
        //menu Fichier
        item1 = new JMenuItem("Fermer");
        menu1.add(item1);
        //menu conference
        item2 = new JMenuItem("Voir la liste des conférences");
        menu2.add(item2);
        //autre option de menu conference
        item2 = new JMenuItem("Ajouter une conférence");
        menu2.add(item2);
        //titre de la fenetre
        this.setTitle(_title);
        //taille de la fenetre par defaut
        this.setSize(700, 500);
        //emplacement de la fentre par defaut 
        this.setLocationRelativeTo(null);
        //Terminer le processus lorsuq'on clique sur la croix rouge
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Definition de la couleur du jpanel
        //pan.setBackground(Color.ORANGE);        
        //rendre la fenetre visible car elle est invisible par defaut 
        this.setVisible(true);
        item1.addActionListener(this);
        item2.addActionListener(this);
//        item3.addActionListener(this);
        Home Accueil = new Home();
        this.setContentPane(Accueil);
        this.setVisible(true);
        this.repaint();
        this.revalidate();

        //Avertir notre JFrame que ce sera notre JPanel qui constituera son content pane. 
       
         }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == item1) {
            System.exit(0);
        }
    }

}
