package LesConferencesDuJeudi;

import Database.POJO.Conference;
import Models.ConferenceModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Anissa
 */
public class Home extends JPanel {
    private int n = 0;
    private JLabel label2 = new JLabel("");
    private JButton boutonClicSurprise;
    private Conference conf1;
    private Conference conf2;
    private Conference conf3;
    private Calendar cal = Calendar.getInstance();
    private ArrayList<Conference> listeConf = new ArrayList<>();

    public Home() {
        super();
            
        JLabel label = new JLabel("Bienvenue sur l'application");
        this.add(label);
        this.add(label2);
       
        JButton boutonClicSurprise = new JButton("CLIC & surprise !");
//        boutonClicSurprise.setBounds(150,40,70,37);
 
      
      boutonClicSurprise.addActionListener(new ActionListener(){
      
        //  this.setVisible(true);
    
    //variable compteur
   
    @Override
    public void actionPerformed(ActionEvent e) {
        n++;
        label2.setText("Vous avez cliqué " + n + "Fois");
       
    }
    
});
    //  boutonClicSurprise.addActionListener((ActionListener) this);
    this.add(boutonClicSurprise);
    conf1=new Conference(1, "titre", cal,7, 14, 2, "Conferencier 1er");
    conf2=new Conference(2, "titre conf 2 ", cal,7, 14, 2, "XXX YYYY");
    conf3=new Conference(3, "titre con 3", cal,7, 14, 2, "Conferencier 3");
    listeConf.add(conf1);
    listeConf.add(conf2);
    listeConf.add(conf3);
    ConferenceModel confMod1=new ConferenceModel(listeConf);
    JTable jTable1 =new JTable(confMod1);
    JScrollPane jScroll1=new JScrollPane(jTable1);
    this.add(jScroll1);
    
    
    
    
}
//    public void setBounds(int x, int y, int width, int height){
//        
//    }
}
