/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LesConferencesDuJeudi;

import Database.DatabaseUtilities;
import com.mysql.jdbc.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger; 

/**
 *
 * @author Anissa
 */
public class LesConferencesDuJeudi {
    public static void main (String[] args){ 
        try {
            CustomWindow fen=new CustomWindow("APPLICATION AD ");
            //DatabaseUtilities.getConnexion();
            Connection c=DatabaseUtilities.getConnexion();
            String sql = "SELECT * FROM conference";
            ResultSet  resultat =DatabaseUtilities.exec(sql);
            while (resultat.next()) {
                int idConferenceJ = resultat.getInt(1);//on peut mettre soit le nom de la colonne soit le num de colonne et le mieux le nom
                String titreConferenceJ = resultat.getString(2);
                java.sql.Date dateConferenceJ = resultat.getDate(3);
                int idConferencierJ = resultat.getInt(4);
                int idSalleJ = resultat.getInt(5);
                int idThemeJ = resultat.getInt(6);
                System.out.println("id : " + idConferenceJ + " Titre : " + titreConferenceJ + " Date : " + dateConferenceJ);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LesConferencesDuJeudi.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
