package Models;

import Database.POJO.Conference;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author Anissa
 */
public class ConferenceModel extends AbstractTableModel {
    
    public final String[] columnsName = {"Titre", "Date", "Conférencier"};
    private ArrayList<Conference> listeConf = new ArrayList<>();
    //constructeur avec une ArrayList comme paramètre

    public ConferenceModel(ArrayList<Conference> _listeConf) {
        this.listeConf = _listeConf;
    }

    @Override
    public int getRowCount() {

        return listeConf.size();
    }

    @Override
    public String getColumnName(int index) {

        return columnsName[index];
    }

    @Override
    public int getColumnCount() {

        return columnsName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conference l = listeConf.get(rowIndex);
        switch (columnIndex) {
            case 0: {
                return l.getTitreConference();
            }
            case 1: {
                return l.getDateString();
            }
            case 2: {
                return l.getNomConference();
            }
            default:
                return "";

        }

    }

}
